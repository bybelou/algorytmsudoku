package sudoku.api;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import sudoku.algorytm.OpisPlanszy;

public class Ramka extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private final static String tytul = "Sudoku";
	private Dimension rozdzielczosc;
	//private Dimension rozmiarOkna = new Dimension(400,400);
	private Dimension rozmiarOkna = new Dimension(402,405);
	private JButton przycisk;
	private Panel panel;

	public Ramka() {
		super(tytul);
		
		rozdzielczosc = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(rozmiarOkna);
		
		Point polozenie = 
				new Point((int) (rozdzielczosc.getWidth()/2 - rozmiarOkna.getWidth()/2),
						(int) (rozdzielczosc.getHeight()/2 - rozmiarOkna.getHeight()/2));
		this.setLocation(polozenie);
		
		przycisk = new JButton("Szukaj Rozwiązania");
		
		przycisk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				OpisPlanszy.ustawDaneWejsciowe();
				OpisPlanszy.uruchomRozwiazywanie();
				
				/*System.out.println(getSize().getWidth()
						+ " "
						+ getSize().getHeight());*/
			}
		});
		
		this.add(przycisk, BorderLayout.SOUTH);
		
		panel = new Panel();
		
		this.add(panel);
		
//		this.pack();
	}

}
