package sudoku.api;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JTextField;

import sudoku.algorytm.OpisPlanszy;

public class Panel extends JPanel{
	
	private static final long serialVersionUID = 1L;

	Panel(){
		super();
		
		this.setBackground(Color.WHITE);
		
		new OpisPlanszy();
		
		this.setLayout(new GridLayout(9,9));
				
		ustawPolaTekstoweNaPlanszy();
		
		this.setSize(386,341);
	}
	

	private void ustawPolaTekstoweNaPlanszy(){
		JTextField t;
		
		for (int i=0; i<9; i++){
			for (int j=0; j<9; j++){
				t = new JTextField();
				this.add(t);
				
				OpisPlanszy.ustawPoleTekstowe(t, i, j);
				
//				System.out.println((i/3*3) + j/3);
			}
		}
	}
	
	public void paintComponent(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		
//		Toolkit t = Toolkit.getDefaultToolkit();
//		this.getWidth();

		g2d.setColor(Color.RED);
		int i=0;
		int j=0;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.BLUE);
		i=1;
		j=0;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.YELLOW);
		i=2;
		j=0;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.GREEN);
		i=0;
		j=1;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.CYAN);
		i=1;
		j=1;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.DARK_GRAY);
		i=2;
		j=1;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.YELLOW);
		i=0;
		j=2;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.BLUE);
		i=1;
		j=2;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.setColor(Color.RED);
		i=2;
		j=2;
		g2d.drawRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		g2d.fillRect(i*this.getWidth()/3, j*this.getHeight()/3, this.getWidth()/3, this.getHeight()/3);
		
		/*Toolkit t = Toolkit.getDefaultToolkit();
		System.out.println(t.getScreenSize().width
				+ " "
				+ t.getScreenSize().height);*/
		/*System.out.println(this.getSize().getWidth()
				+ " "
				+ this.getSize().getHeight());*/
	}
}
