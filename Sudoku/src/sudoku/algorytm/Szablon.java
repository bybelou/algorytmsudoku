package sudoku.algorytm;

import java.util.ArrayList;

import javax.swing.JTextField;

public class Szablon {
	private final int wiersz;
	private final int kolumna;
	private JTextField poleTekstowe;
	private int kwadratSudoku;
	private int liczbaPola = 0;
	private boolean wartoscPoczatkowa=false;
	private ArrayList<Integer> listaMozliwychWartosci; 
	
	Szablon(int w, int k){
		this.wiersz = w;
		this.kolumna = k;
		
		listaMozliwychWartosci = new ArrayList<Integer>();
		for (int i=1; i<=9; i++){
			listaMozliwychWartosci.add(new Integer(i));
		}
	}
	
	@Override
	public String toString() {
		//return super.toString();
		return "WIERSZ:" + Integer.toString(this.wiersz)
				+ "\tKOLUMNA:" + Integer.toString(this.kolumna);
	}
	
	public void ustawPoleTekstowe(JTextField t){
		this.poleTekstowe = t;
	}
	
	public void ustawKwadrat(int k){
		this.kwadratSudoku = k;
	}
	
	public void ustawWartoscPolaTestowego(){
//		this.poleTekstowe.setText("W:" + this.wiersz + " - " + "K:" + this.kolumna);
//		this.poleTekstowe.setText("[" + this.kwadratSudoku + "] " + "W:" + this.wiersz + " - " + "K:" + this.kolumna);
		this.poleTekstowe.setText("[" + this.kwadratSudoku + "] ");
	}
	
	public int pobierzKolumne(){
		return this.kolumna;
	}
	
	public int pobierzWiersz(){
		return this.wiersz;
	}
	
	public void ustawLiczbeWejsciowa(){
		try{
			if (poleTekstowe.getText().length()>0){
				int l = Integer.parseInt(poleTekstowe.getText());
				
				if (l>0 && l<=9){
					liczbaPola = l;
					wartoscPoczatkowa = true;
					
					poleTekstowe.setEditable(false);
					listaMozliwychWartosci.clear();
//					poleTekstowe.setText("<b>" + poleTekstowe.getText() + "<b>");
				}
			} else {
				drukujMozliweWartosci();
			}
		}catch(NumberFormatException e){
			System.out.println("bledna wartosc w " + "W:" + this.wiersz + " - " + "K:" + this.kolumna);
		}
	}
	
	private void drukujMozliweWartosci(){
		String wartosci = "";
		for (Integer liczba : listaMozliwychWartosci){
			if (wartosci != ""){
				wartosci += ",";
			}
			wartosci += liczba.toString();
			poleTekstowe.setText(wartosci);
		}
	}
	
	private void drukujMozliweWartosciLubPole(){
		if (liczbaPola>0){
			poleTekstowe.setText(Integer.toString(liczbaPola));
		}else{
			String wartosci = "";
			for (Integer liczba : listaMozliwychWartosci){
				if (wartosci != ""){
					wartosci += ",";
				}
				wartosci += liczba.toString();
				poleTekstowe.setText(wartosci);
			}
		}
	}
	
	public int pobierzLiczbePola(){
		return this.liczbaPola;
	}
	
	public void usunPotencjalnaWartosc(int liczba){
		for (int i=0; i<listaMozliwychWartosci.size(); i++){
			if (liczba == listaMozliwychWartosci.get(i)){
				listaMozliwychWartosci.remove(i);
				
				if (listaMozliwychWartosci.size()==1){
					liczbaPola = listaMozliwychWartosci.get(0);
				}

//				drukujMozliweWartosci();
				drukujMozliweWartosciLubPole();
			}
		}
	}
	
	public int pobierzKwadratSudoku(){
		return this.kwadratSudoku;
	}
	
	/*public void ustawListe(){
		
	}*/
}
