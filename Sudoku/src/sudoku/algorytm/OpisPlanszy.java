package sudoku.algorytm;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JTextField;

public final class OpisPlanszy {
	static private ArrayList<Szablon> pola;
//	private ArrayList<Szablon> pola;
	
	public OpisPlanszy() {
//		System.out.println("poczatek");
		
		pola = new ArrayList<Szablon>();
		
		for (int i=0; i<9; i++){
			for (int j=0; j<9; j++){
				pola.add(new Szablon(i, j));
			}
		}

		/*pola.add(new Szablon(0, 0));
		pola.add(new Szablon(0, 1));
		pola.add(new Szablon(0, 2));
		pola.add(new Szablon(0, 3));
		pola.add(new Szablon(0, 4));
		pola.add(new Szablon(0, 5));
		pola.add(new Szablon(0, 6));
		pola.add(new Szablon(0, 7));
		pola.add(new Szablon(0, 8));

		pola.add(new Szablon(1, 0));
		pola.add(new Szablon(1, 1));
		pola.add(new Szablon(1, 2));
		pola.add(new Szablon(1, 3));
		pola.add(new Szablon(1, 4));
		pola.add(new Szablon(1, 5));
		pola.add(new Szablon(1, 6));
		pola.add(new Szablon(1, 7));
		pola.add(new Szablon(1, 8));

		pola.add(new Szablon(2, 0));
		pola.add(new Szablon(2, 1));
		pola.add(new Szablon(2, 2));
		pola.add(new Szablon(2, 3));
		pola.add(new Szablon(2, 4));
		pola.add(new Szablon(2, 5));
		pola.add(new Szablon(2, 6));
		pola.add(new Szablon(2, 7));
		pola.add(new Szablon(2, 8));

		pola.add(new Szablon(3, 0));
		pola.add(new Szablon(3, 1));
		pola.add(new Szablon(3, 2));
		pola.add(new Szablon(3, 3));
		pola.add(new Szablon(3, 4));
		pola.add(new Szablon(3, 5));
		pola.add(new Szablon(3, 6));
		pola.add(new Szablon(3, 7));
		pola.add(new Szablon(3, 8));

		pola.add(new Szablon(4, 0));
		pola.add(new Szablon(4, 1));
		pola.add(new Szablon(4, 2));
		pola.add(new Szablon(4, 3));
		pola.add(new Szablon(4, 4));
		pola.add(new Szablon(4, 5));
		pola.add(new Szablon(4, 6));
		pola.add(new Szablon(4, 7));
		pola.add(new Szablon(4, 8));

		pola.add(new Szablon(5, 0));
		pola.add(new Szablon(5, 1));
		pola.add(new Szablon(5, 2));
		pola.add(new Szablon(5, 3));
		pola.add(new Szablon(5, 4));
		pola.add(new Szablon(5, 5));
		pola.add(new Szablon(5, 6));
		pola.add(new Szablon(5, 7));
		pola.add(new Szablon(5, 8));

		pola.add(new Szablon(6, 0));
		pola.add(new Szablon(6, 1));
		pola.add(new Szablon(6, 2));
		pola.add(new Szablon(6, 3));
		pola.add(new Szablon(6, 4));
		pola.add(new Szablon(6, 5));
		pola.add(new Szablon(6, 6));
		pola.add(new Szablon(6, 7));
		pola.add(new Szablon(6, 8));

		pola.add(new Szablon(7, 0));
		pola.add(new Szablon(7, 1));
		pola.add(new Szablon(7, 2));
		pola.add(new Szablon(7, 3));
		pola.add(new Szablon(7, 4));
		pola.add(new Szablon(7, 5));
		pola.add(new Szablon(7, 6));
		pola.add(new Szablon(7, 7));
		pola.add(new Szablon(7, 8));

		pola.add(new Szablon(8, 0));
		pola.add(new Szablon(8, 1));
		pola.add(new Szablon(8, 2));
		pola.add(new Szablon(8, 3));
		pola.add(new Szablon(8, 4));
		pola.add(new Szablon(8, 5));
		pola.add(new Szablon(8, 6));
		pola.add(new Szablon(8, 7));
		pola.add(new Szablon(8, 8));*/
		
		
//		System.out.println("srodek");
		
//		pobierzPlansze();

//		System.out.println("koniec");
	}
	
	private void pokazPlansze(){
		Iterator<Szablon> iterator = pola.iterator();
		
		while (iterator.hasNext()){
//			Szablon s = (Szablon) iterator.next();
//			System.out.println(s);

			System.out.println(iterator.next());
		}
	}
	
	public static ArrayList<Szablon> pobierzPlansze(){
		return pola;
	}
	
	public static void ustawPoleTekstowe(JTextField t, int w, int k){
		/*for (Szablon s : pola){
			System.out.println(s.toString());
			
		}*/
		for (Szablon pol : pola){
			if (pol.pobierzWiersz()==w && pol.pobierzKolumne()==k){
				pol.ustawPoleTekstowe(t);

				pol.ustawKwadrat((w/3*3) + k/3);
				
//				pol.ustawWartoscPolaTestowego();
				
			}
		}	
	}
	
	public static void ustawDaneWejsciowe(){
		for (Szablon pol : pola){
			pol.ustawLiczbeWejsciowa();
		}
	}
	
	public static void uruchomRozwiazywanie(){
		for (Szablon pol : pola){
			//pol.ustawListe();
			
			if (pol.pobierzLiczbePola()==0){
				//przeszukiwanie wierszy
				for (Szablon wiersz : pola){
					if (!pol.equals(wiersz) && pol.pobierzWiersz() == wiersz.pobierzWiersz() && wiersz.pobierzLiczbePola()>0){
						//jesli jest uzupelniona wartosc to usun ja z listy
						pol.usunPotencjalnaWartosc(wiersz.pobierzLiczbePola());
					}
				}
				
				//przeszukiwanie kolumn
				for (Szablon kolumna : pola){
					if (!pol.equals(kolumna) && pol.pobierzKolumne() == kolumna.pobierzKolumne() && kolumna.pobierzLiczbePola()>0){
						//jesli jest uzupelniona wartosc to usun ja z listy
						pol.usunPotencjalnaWartosc(kolumna.pobierzLiczbePola());
					}
				}
				
				//przeszukiwanie kwadratow
				for (Szablon kwadrat : pola){
					if (!pol.equals(kwadrat) && pol.pobierzKwadratSudoku() == kwadrat.pobierzKwadratSudoku() && kwadrat.pobierzLiczbePola()>0){
						//jesli jest uzupelniona wartosc to usun ja z listy
						pol.usunPotencjalnaWartosc(kwadrat.pobierzLiczbePola());
					}
				}
			}
			
		}
	}

}
